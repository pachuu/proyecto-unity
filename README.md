## UNLIT SOULS - A ARPG PROTOTYPE GAME

Created and developed by Juan Martin Pithod and Pablo R Pizarro

Prototype created for our Senior Technician in Software Programming career at UTN FRM (Universidad Técnologica Nacional - Regional Mendoza)

- Genre: Medieval-Fantastic 3D ARPG
- Platform: PC
- Age: 16+
- Execution Time: 6 month
- Language: Spanish

---
**What technologies, libraries and methodologies we were used :**
- Unity 3D Engine
- C#
- OOP
- Json
- SOLID
- UniRX (reactive programming)
- Agile | Scrum | Kanban


