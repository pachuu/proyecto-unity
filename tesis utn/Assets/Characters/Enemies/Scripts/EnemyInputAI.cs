﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Characters.Scripts.Components;
using DataContainers;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class EnemyInputAI : MonoBehaviour, IInputStream
{
    // Use this for initialization
    private GameObject _target;
    private GameObject _base;
    private GameObject _player;
    private IDisposable _targetMovement;
    private IReceiveDamage _receiveDamage;
    private EnemyStatusController _enemyStatus;
    public Action<GameObject, Vector3> OnAttackButton { get; set; }
    public Action<Vector3> OnNewPosition { get; set; }
    public Action<GameObject, Vector3> OnInteractButton { get; set; }

    public Coroutine DistanceCalc;
    public bool EnableAi = true;


    private void Awake()
    {
        _receiveDamage = GetComponent<IReceiveDamage>();

        //todo: Arreglar refencia
        _enemyStatus = GetComponent<EnemyStatusController>();
    }

    private void Start()
    {
        //todo descomentar en prod    
        _target = _base = GameObject.FindGameObjectWithTag("Base");
        _player = GameObject.FindGameObjectWithTag("Player");

        if (EnableAi)
            InitInputs();

        DistanceCalc = StartCoroutine(CalculatePlayerDistance());
        _enemyStatus.OnDead += delegate { StopCoroutine(DistanceCalc); };
        GameManager.Instance.OnGameEnd += OnGameEnd;
    }

    private void OnGameEnd()
    {
        StopCoroutine(DistanceCalc);
        StopInputs();
    }


    public void InitInputs()
    {
        if (_receiveDamage != null)
        {
            _receiveDamage.OnDead += () => { _targetMovement.Dispose(); };
        }

        _targetMovement = Observable.EveryFixedUpdate().Subscribe(_ =>
        {
            if (_target != null)
            {
                OnInteractButton(_target, _target.transform.position);
            }
        }); // OPTIMIZAR solo se movian cuando la distancia era mayor a la de ataque, podia moverme alrededor del bicho si estaba adentro del rango
    }

    public void StopInputs()
    {
        _targetMovement.Dispose();
    }

    private void OnTargetNewPosition(Vector3 targetPosition)
    {
        var distance = (targetPosition - gameObject.transform.position).magnitude;
        if (distance > _enemyStatus.data.AttackRange)
        {
            OnNewPosition?.Invoke(targetPosition);
        }
        else
        {
            //OnAttackButton?.Invoke(_target.);
        }
    }

    private IEnumerator CalculatePlayerDistance()
    {
        yield return new WaitUntil(() => _enemyStatus.data != null);
        while (true)
        {
            var distance = (_player.transform.position - transform.position).magnitude;
            if (distance <= _enemyStatus.data.VisionRange)
            {
                _target = _player;
            }
            else
            {
                _target = _base;
            }

            yield return new WaitForSeconds(1);
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnGameEnd -= OnGameEnd;
    }
}