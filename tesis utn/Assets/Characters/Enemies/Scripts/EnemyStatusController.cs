﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters;
using Characters.Scripts.Components;
using DataContainers;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStatusController : MonoBehaviour, IReceiveDamage, IRecoverHealth
{
    // Use this for initialization
    public EnemyData data;
    [SerializeField] private CharacterGUIController _characterGuiController;
    private IAnimatorService _animatorService;
    [SerializeField] private CharacterSystem _characterSystem;
    [SerializeField] private NavMeshAgent _agent;
    [Range(0, 1)] [SerializeField] private float _runAnimationSpeedFactor;
    public Action OnDead { get; set; }
    public Action<float> OnReceiveDamage { get; }

    public bool dead { get; set; }

    private void Awake()
    {
        _animatorService = GetComponent<IAnimatorService>();
    }

    private void Start()
    {
//        InitStats();
    }

    public void InitStats(EnemyData data)
    {
        //HardCoded
        this.data = data;
        _characterGuiController.SetMaxSliderHealth(data.Health);
        AddAttackSpeed(0);
        AddMovementSpeed(data.MovementSpeed);
        _agent.speed = data.MovementSpeed;
    }

    private void AddAttackSpeed(float amount)
    {
        data.Agility += amount;
        _animatorService.animator.SetFloat("AttackSpeed", data.Agility);
    }

    private void AddMovementSpeed(float amount)
    {
        data.MovementSpeed += amount;
        _agent.speed = data.MovementSpeed;
        _animatorService.animator.SetFloat("MovementSpeed", (data.MovementSpeed / 3));
    }


    public void ReduceHealth(float amount)
    {
        if (data.Health > 0)
        {
            this.data.Health -= amount;
            this._characterGuiController.SetSliderHealth(this.data.Health);
            this._characterGuiController.ShowSlider(true);
            OnReceiveDamage?.Invoke(amount);
        }

        if (data.Health <= 0)
        {
            this._characterGuiController.SetSliderHealth(0);
            EnemyKilled();
        }
    }

    private void EnemyKilled()
    {
        //test
        this.data.Health = data.MaxHealth;
        this._characterGuiController.SetSliderHealth(data.MaxHealth);
        _animatorService.SetTriggerAnimation("onDie");
        _agent.isStopped = true;
        _characterGuiController.ShowSlider(false);
        dead = true;
        OnDead?.Invoke();

        //

        //TODO: comunicar con responsable para sumar puntaje, contador de enemigos,etc.
        //TODO: Eliminar enemigo (o poner en un pool de enemigos);
        Debug.Log("ENEMIGO DESTRUIDO");
    }

    public Action OnFullHealth { get; }
    public Action OnRecoverHealth { get; }

    public void RecoverHealth(float amount)
    {
        throw new NotImplementedException();
    }
}