﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using Characters.Scripts.Components;
using DataContainers;
using UnityEngine;

public class NPCSystem : CharacterSystem
{
    private IAnimatorService _animatorService;
    private Animator _animator;
    private DayCicleController _dayCicleController;
    private void Awake()
    {
        _dayCicleController = FindObjectOfType<DayCicleController>();
        if (_dayCicleController != null)
        {
            _dayCicleController.OnDay += OnDay;
            _dayCicleController.OnNight += OnNight;
        }
        _animator = GetComponent<Animator>();
        _animatorService = GetComponent<IAnimatorService>();
        _animatorService.InjectAnimator(_animator);
    }

    private void OnNight()
    {
        //todo: Mover el personaje para que invoke en la piedra magica
    }

    private void OnDay()
    {
        //todo: Mover el personaje hacia la entrada. Para que sea interactuable.
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    protected override void Init(CharacterData data)
    {
        throw new System.NotImplementedException();
    }

    public override void OnCharacterDestroy()
    {
        throw new System.NotImplementedException();
    }


}