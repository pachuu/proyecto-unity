﻿using System.Collections;
using System.Collections.Generic;
using Characters.Scripts.Components;
using DataContainers;
using Enums;
using UnityEngine;
using UnityEngine.UI;

public class SkillManager : MonoBehaviour
{
    [SerializeField] private Button _btnSkill1;
    [SerializeField] private Button _btnSkill2;
    [SerializeField] private Button _btnSkill3;

    [SerializeField] private GameObject _skill1;
    [SerializeField] private GameObject _skill2;
    [SerializeField] private GameObject _skill3;
    private PlayerStatusController _playerStatus;
    private IRecoverHealth _recoverHealth;

    private Skill healthSkill;
    private Skill electricBuffSkill;
    private Skill fireCircleSkill;

    private List<Skill> _skills;

    // Use this for initialization
    private void Awake()
    {
//        CreateSkills();
    }

    void Start()
    {
//        GameManager.Instance.OnInitMenu += OnInitMenu;
//        GameManager.Instance.OnInitGame += OnInitGame;


        //_btnSkill1.onClick.AddListener(delegate { _skill1.SetActive(true); });
//		_btnSkill2.onClick.AddListener(delegate { _skill2.SetActive(true); });
//		_btnSkill3.onClick.AddListener(delegate { _skill3.SetActive(true); });
    }

    private void OnInitGame()
    {
        //class start method
    }

    public void CreateSkills()
    {
        healthSkill = new Skill();
        healthSkill.skillName = "HEALTH";
        healthSkill.amount = 20;
        healthSkill.lvlToUnblock = 1;
        healthSkill.viewButton = _btnSkill1;
        healthSkill.SkillGameObjectView = _skill1;
        healthSkill.maxCooldownTime = 10;
        healthSkill.SetSkillController();

        electricBuffSkill = new Skill();
        electricBuffSkill.skillName = "ElectrikAttack";
        electricBuffSkill.amount = 15;
        electricBuffSkill.activeTime = 13;
        electricBuffSkill.lvlToUnblock = 3;
        electricBuffSkill.SkillGameObjectView = _skill2;
        electricBuffSkill.maxCooldownTime = 25;
        electricBuffSkill.isPassive = true;
        electricBuffSkill.viewButton = _btnSkill2;
        electricBuffSkill.SetSkillController();

        fireCircleSkill = new Skill();
        fireCircleSkill.skillName = "RingFire";
        fireCircleSkill.activeTime = 10;
        fireCircleSkill.amount = 2;
        fireCircleSkill.lvlToUnblock = 6;
        fireCircleSkill.SkillGameObjectView = _skill3;
        fireCircleSkill.maxCooldownTime = 20;
        fireCircleSkill.isPassive = true;
        fireCircleSkill.viewButton = _btnSkill3;
        fireCircleSkill.SetSkillController();


        _skills = new List<Skill> {healthSkill, electricBuffSkill, fireCircleSkill};
    }

    private void OnLevelUp(int level)
    {
        foreach (var s in _skills)
        {
            if (level >= s.lvlToUnblock)

                UnlockSkill(s);
        }
    }

    private void UnlockSkill(Skill skill)
    {
        skill.unlocked = true;
        skill.viewButton.interactable = true;
    }

    public void InitSkills()
    {
        _recoverHealth = GetComponent<IRecoverHealth>();
        _playerStatus = GetComponent<PlayerStatusController>();
        _playerStatus.OnLevelUp += OnLevelUp;
        CreateSkills();
        OnLevelUp(_playerStatus._data.Level);
    }


    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.GameMode == GameState.Game)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                UseSkill(healthSkill);
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                UseSkill(electricBuffSkill);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                UseSkill(fireCircleSkill);
            }
        }
    }

    private void UseSkill(Skill skill)
    {
        if (skill.unlocked && skill.cooldownTime == 0)
        {
            if (skill.isPassive)
            {
                StartCoroutine(ActivePassiveSkill(skill));
            }
            else
            {
                StartCoroutine(CoolDownSkill(skill));
            }

            skill.GetController().InitSkill(_playerStatus, skill.amount);
        }
    }

    private IEnumerator ActivePassiveSkill(Skill skill)
    {
        if (skill.active) yield break;
        var maxActiveTime = skill.activeTime;
        skill.active = true;
        while (skill.activeTime > 1)
        {
            skill.activeTime--;
            skill.SetTextCounter(skill.activeTime);
//            Debug.Log("PASSIVE ACTIVE->" + skill.skillName + "<time> -> " + skill.activeTime);
            yield return new WaitForSeconds(1);
        }

        skill.GetController().StopSkill();
        skill.activeTime = maxActiveTime;
        skill.active = false;

        StartCoroutine(CoolDownSkill(skill));
    }

    private IEnumerator CoolDownSkill(Skill skill)
    {
        if (skill.active) yield break;
        skill.active = true;
        skill.cooldownTime = skill.maxCooldownTime;
        skill.viewButton.interactable = false;
        while (skill.cooldownTime > 0)
        {
            skill.cooldownTime--;
            skill.SetTextCounter(skill.cooldownTime);
//            Debug.Log("COOLD DOWN SKILL ->" + skill.skillName + "<time> -> " + skill.cooldownTime);
            yield return new WaitForSeconds(1);
        }

        skill.viewButton.interactable = true;
        skill.active = false;
        skill.GetController().StopSkill();
    }
}