﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters;
using Characters.Scripts;
using Characters.Scripts.Components;
using DataContainers;
using Enums;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSystem : CharacterSystem
{
    [Header("Sub-systems")] [SerializeField]
    private MouseInput _mouseInput;

    [SerializeField] private PlayerData data;
    private PlayerStatusController _playerStatus;
    private EnemySystem _currentEnemy;
    private CharacterGUIController _characterGuiController;
    private SkillManager _skillManager;
    private CombatController _combatController;
    private IAnimatorService _animator;
    [SerializeField] private Animator prefabAnimator;

    [SerializeField] private Transform _playerMenuPosition;

    [Space(20)] Vector3 posToMove;
    IEnumerator PlayerMovement;
    [SerializeField] private GameObject _currTarget;
    private PlayerState _state = PlayerState.Idle;

    // Use this for initialization
    void Awake()
    {
        _combatController = GetComponent<CombatController>();
        _animator = GetComponent<IAnimatorService>();
        _animator.InjectAnimator(prefabAnimator);
        _playerStatus = GetComponent<PlayerStatusController>();
        _skillManager = GetComponent<SkillManager>();
    }

    private void Start()
    {
        //todo: comentar en produccion
        //SetupPlayer(data);

        GameManager.Instance.OnInitMenu += OnInitMenu;
        GameManager.Instance.OnInitGame += OnInitGame;

        _playerStatus.OnDead += OnDead;
    }

    private void OnDead()
    {
        _mouseInput.StopInputs();
        GameManager.Instance.EndGame();
    }

    private void OnInitMenu()
    {
        _mouseInput?.StopInputs();
        transform.position = _playerMenuPosition.position;
        transform.rotation = _playerMenuPosition.rotation;
    }

    private void OnInitGame()
    {
        _mouseInput?.InitInputs();
        _animator.animator.ResetTrigger("onDie");
    }

    public void SetupPlayer(PlayerData data)
    {
        var dataInstance = Instantiate(data);
        _playerStatus.InitStats(dataInstance);
        _combatController.SetData(dataInstance);
        _skillManager.InitSkills();
    }


#if UNITY_EDITOR
    private void OnValidate()
    {
        if (_characterGuiController == null)
        {
            this._characterGuiController = GetComponent<CharacterGUIController>();
        }
    }
#endif


    protected override void Init(CharacterData data)
    {
    }

    public override void OnCharacterDestroy()
    {
        //throw new NotImplementedException();
    }

    public void EnemyKilled(EnemyData data)
    {
        _playerStatus.AddExp(data.givenExperience);
        _playerStatus.AddPoints(data.givenExperience * 0.5f);
    }
}