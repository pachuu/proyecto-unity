﻿using System.Collections;
using System.Collections.Generic;
using DataContainers;
using UnityEngine;
using UnityEngine.AI;

public abstract class CharacterSystem : MonoBehaviour {
    
    protected abstract void Init(CharacterData data);
    public abstract void OnCharacterDestroy();
}
