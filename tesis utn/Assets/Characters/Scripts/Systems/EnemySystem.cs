﻿using System;
using Characters.Scripts.Components;
using DataContainers;
using UnityEngine;

namespace Characters.Scripts
{
    public class EnemySystem : CharacterSystem, IInteractable
    {
        // Use this for initialization
//        [SerializeField] private EnemyData _rangedEnemy;
        [SerializeField] private EnemyData _data;
        private PlayerSystem _player;
        private Coroutine _lookForPlayer;
        private Coroutine _chasePlayer;
        [SerializeField] private EnemyStatusController _enemyStatus;
        private CombatController _combatController;
        [SerializeField] private Animator _prefabAnimator;
        [SerializeField] private Outline _enemyOutline;
        private IReceiveDamage _receiveDamage;
        [SerializeField] private Collider _charCollider;
        [SerializeField] private CharacterGUIController _characterGuiController;
        private MeshRenderer _mesh;

        private IAnimatorService _animator;
        public Action _onEnemyKilled;

        [SerializeField] private GameObject enemyMarkerPrefab;
        [SerializeField] private Vector3 enemyMarkerPosition;
        private GameObject enemyMarker;


        private void Awake()
        {
            _animator = GetComponent<IAnimatorService>();
            _animator.InjectAnimator(_prefabAnimator);
            _receiveDamage = GetComponent<IReceiveDamage>();
//            _mesh = GetComponent<MeshRenderer>();
            _combatController = GetComponent<CombatController>();
            _enemyStatus = GetComponent<EnemyStatusController>();

            enemyMarker = Instantiate(enemyMarkerPrefab, gameObject.transform.position + enemyMarkerPosition,
                Quaternion.identity);
            enemyMarker.transform.parent = gameObject.transform;
        }

        private void Start()
        {
            Init(null);
            _player = FindObjectOfType<PlayerSystem>();

            GameManager.Instance.OnInitMenu += OnInitMenu;
            GameManager.Instance.OnInitGame += OnInitGame;

            GameManager.Instance.OnGameEnd += OnGameEnd;
            //todo: harcodeado, se deberian instanciar dependiendo de la oleada o el nivel del jugador
        }

        private void OnGameEnd()
        {
            GameManager.Instance.OnInitMenu -= OnInitMenu;
            GameManager.Instance.OnInitGame -= OnInitGame;
        }

        private void OnInitGame()
        {
            if (gameObject.activeInHierarchy == false) return;
            Destroy(gameObject);
        }

        private void OnInitMenu()
        {
            //todo: descomentar en produccion
            Destroy(gameObject);
        }

        protected override void Init(CharacterData characterData)
        {
            //todo: se deberia setear desde afuera.
            var data = Instantiate(_data);

            _combatController.SetData(data);
            _enemyStatus.InitStats(data);
            if (_receiveDamage != null)
            {
                _receiveDamage.OnDead += OnCharacterDestroy;
            }
        }

        public override void OnCharacterDestroy()
        {
            _onEnemyKilled?.Invoke();
            _charCollider.enabled = false;
            enemyMarker.gameObject.SetActive(false);
            _player.EnemyKilled(_data);
            SpawnManager.Instance.KillEnemy(gameObject);
            Destroy(gameObject, 3);
        }


//        public void SetTarget(Color color)
//        {
//            StartCoroutine(ChangeColor(color));
//        }
//
//        private IEnumerator ChangeColor(Color color)
//        {
//            var material = _mesh.materials[0];
//            var oldColor = material.color;
//            material.color = color;
//
//            yield return new WaitForSeconds(.1f);
//
//            material.color = oldColor;
//        }

        public void OnInteracted()
        {
        }

        public GameObject GameObject()
        {
            return gameObject;
        }

        private void OnMouseEnter()
        {
            Debug.Log("MOUSE ENTER");
            MouseUI.Instance.ChangeCursor(CursorType.attack);
            if (_enemyOutline == null)
                _enemyOutline = GetComponent<Outline>();

            _enemyOutline.enabled = true;
        }


        private void OnMouseExit()
        {
            MouseUI.Instance.ChangeCursor(CursorType.normal);
            _enemyOutline.enabled = false;
        }
        
        private void OnDestroy()
        {
            GameManager.Instance.OnInitGame -= OnInitGame;
            GameManager.Instance.OnGameEnd -= OnGameEnd;
        }
    }
}