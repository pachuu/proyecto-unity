﻿using System;
using UnityEngine;

namespace Characters.Scripts.Components
{
    public interface IInteractable
    {
        void OnInteracted();
        GameObject GameObject();
    }
}