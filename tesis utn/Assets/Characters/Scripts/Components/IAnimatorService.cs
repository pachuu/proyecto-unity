﻿using System;
using UnityEngine;

namespace Characters.Scripts.Components
{
    public interface IAnimatorService
    {
        void InjectAnimator(Animator animator);
        Animator animator { get; set; }
        void PlayAnimation(string parameterName, bool state);
        void SetTriggerAnimation(string parameterName);
        bool GetAnimationState(string animation);
        StateMachineBehaviour ReturnMachineBehaviour();

    }
}