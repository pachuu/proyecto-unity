﻿using System;
using UnityEngine;

namespace Characters.Scripts.Components
{
    public interface IInputStream
    {
        Action<GameObject, Vector3> OnAttackButton { get; set; }
        Action<Vector3> OnNewPosition { get; set; }
        Action<GameObject, Vector3> OnInteractButton { get; set; }
        void InitInputs();
        void StopInputs();
    }
}