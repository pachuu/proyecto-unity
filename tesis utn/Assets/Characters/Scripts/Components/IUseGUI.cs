﻿namespace Characters.Scripts.Components
{
    public interface IUseGUI
    {
        void ShowDamage();
        void SetTarget();
    }
}