﻿using System;
using UniRx;
using UnityEngine;


namespace Characters.Scripts.Components
{
    public interface IMoveable
    {
        Action<Vector3> OnNewPosition { get; set; }
        Action<Vector3> OnPositionChanged { get; set; }
        Action OnPositionReached { get; set; }
        void StopPlayer();
        IObservable<long> MoveToInteract(GameObject enemy, float range);
    }
}