﻿using System;

namespace Characters.Scripts.Components
{
    public interface IReceiveDamage
    {
        Action OnDead { get; set; }
        Action<float> OnReceiveDamage { get; }
        void ReduceHealth(float amount);
        bool dead { get; set; }
    }

    public interface IRecoverHealth
    {
        Action OnFullHealth { get; }
        Action OnRecoverHealth { get; }
        void RecoverHealth(float amount);
    }   
}
