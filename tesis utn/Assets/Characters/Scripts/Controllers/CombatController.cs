﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters.Scripts.Components;
using DataContainers;
using Enums;
using UniRx;
using UnityEngine;

public class CombatController : MonoBehaviour
{
    // Use this for initialization
    private IInputStream _input;
    private IAnimatorService _animator;
    private CharacterMecanimBehaviour _behaviour;
    private IReceiveDamage _currentTarget;
    private IReceiveDamage _status;
    private IMoveable _moveable;
    private CharacterData data;
    private bool playerDead = false;
    private IDisposable _interactableReachedDisposable;

    [SerializeField] private ProjectileController _projectileToUse;
    [SerializeField] private Transform _projectileSocket;

    void Start()
    {
        _input = GetComponent<IInputStream>();
        _animator = GetComponent<IAnimatorService>();
        //data = GetComponent<PlayerStatusController>();
        _moveable = GetComponent<IMoveable>();
        _status = GetComponent<IReceiveDamage>();
        if (_status != null)
        {
            _status.OnDead += () => { _interactableReachedDisposable?.Dispose(); };
        }

        if (_animator != null)
        {
            _behaviour = _animator.ReturnMachineBehaviour() as CharacterMecanimBehaviour;
            if (_behaviour != null)
            {
                _behaviour.OnAttackAnimFinish += OnAttackAnimFinish;
                _behaviour.OnAttackedAnimation += OnAttackedAnimation;
            }
        }

        if (_input != null)
        {
            //_input.OnAttackButton += OnAttack;
            _input.OnInteractButton += OnInteract;
            _input.OnNewPosition += OnNewPosition;
        }
        else
        {
            Debug.LogWarning("Missing reference <IInputStream> in " + gameObject.name);
        }
        GameManager.Instance.OnInitGame += OnInitGame;
        GameManager.Instance.OnGameEnd += OnGameEnd;
    }

    private void OnInitGame()
    {
        playerDead = false;
    }

    private void OnGameEnd()
    {
        playerDead = true;
        _interactableReachedDisposable?.Dispose();
        _animator.PlayAnimation("onAttack", false);
    }

    private void Tm_CollisionEnter(object sender, RFX4_TransformMotion.RFX4_CollisionInfo e)
    {
        print(sender);
        if (e.Hit.transform.gameObject.CompareTag("Player"))
        {
            _currentTarget?.ReduceHealth(data.Damage);
        }
    }

    private void OnAttackedAnimation()
    {
        if (_projectileToUse != null)
        {
            FireProjectile();
        }
        else if (!_currentTarget.dead)
        {
            _currentTarget?.ReduceHealth(data.Damage);
        }
    }

    private void OnAttackAnimFinish()
    {
        _animator.PlayAnimation("onAttack", false);
    }

    private void OnNewPosition(Vector3 pos)
    {
        //_animator.PlayAnimation("onAttack", false);
    }

    private void OnInteract(GameObject obj, Vector3 pos)
    {
        var enemy = obj.GetComponent<IReceiveDamage>();
        if (enemy != null && !enemy.dead)
            OnAttack(obj);
    }

    private void OnAttack(GameObject enemy)
    {
        var heading = enemy.transform.position - transform.position;
        var distance = heading.magnitude;
        var damageable = enemy.GetComponent<IReceiveDamage>();
        if (damageable == null) return;
        if (distance < data.AttackRange)
        {
            Debug.Log("TARGET ESTA EN RANGO");
            _moveable.StopPlayer();
            transform.LookAt(enemy.transform);
            AttackEnemy(damageable);
        }
        else
        {
            Debug.Log("TARGET  NO -> ESTA EN RANGO");
            if (_animator.GetAnimationState("onAttack"))
            {
                _animator.PlayAnimation("onAttack", false);
//                _animator.PlayAnimation("onRun", true);
                return;
            }


            _interactableReachedDisposable?.Dispose();
            _interactableReachedDisposable = _moveable.MoveToInteract(enemy, data.AttackRange)
                .Subscribe(_ =>
                {
                    if (playerDead) _interactableReachedDisposable.Dispose();
                    _moveable.StopPlayer();
                    transform.LookAt(enemy.transform);
                    AttackEnemy(damageable);
                    _interactableReachedDisposable.Dispose();
                });
        }
    }

    private void AttackEnemy(IReceiveDamage enemy)
    {
        _currentTarget = enemy;
        if (!_animator.GetAnimationState("onAttack"))
        {
            _animator.PlayAnimation("onAttack", true);
        }
    }

    private void FireProjectile()
    {
        //todo: lo ideal seria hacer un pool de proyectiles, lo dejo para despues.
        var proj = Instantiate(_projectileToUse, _projectileSocket.transform.position,
            _projectileSocket.transform.rotation);
        proj.transform.parent = gameObject.transform;
        proj.transform.rotation = _projectileSocket.transform.rotation;
        proj.gameObject.SetActive(true);
        proj.SetController(this);
        Destroy(proj.gameObject, 5);
    }

    public void OnProjectileAttack(IReceiveDamage receiveDamage)
    {
        receiveDamage.ReduceHealth(data.Damage);
    }

    public void SetData(CharacterData d)
    {
        data = d;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnInitGame -= OnInitGame;
        GameManager.Instance.OnGameEnd -= OnGameEnd;
    }
}