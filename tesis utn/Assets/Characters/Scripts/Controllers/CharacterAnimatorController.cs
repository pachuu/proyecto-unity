﻿using Characters.Scripts.Components;
using UnityEngine;


namespace Characters
{
    public class CharacterAnimatorController : MonoBehaviour, IAnimatorService
    {
        public void InjectAnimator(Animator animator)
        {
            this.animator = animator;
            _behaviour = animator.GetBehaviour<StateMachineBehaviour>();
        }

        public Animator animator { get; set; }
        private StateMachineBehaviour _behaviour;

        private void Start()
        {
            //animator = GetComponent<Animator>();
            //_behaviour = animator.GetBehaviour<StateMachineBehaviour>();
        }

        public void PlayAnimation(string parameterName, bool state)
        {
            animator?.SetBool(parameterName, state);
        }

        public void SetTriggerAnimation(string parameterName)
        {
            animator?.SetTrigger(parameterName);
        }

        public bool GetAnimationState(string animation)
        {
            return animator.GetBool(animation);
        }

        public StateMachineBehaviour ReturnMachineBehaviour()
        {
            return _behaviour;
        }
    }
}