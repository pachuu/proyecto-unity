﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Characters;
using Characters.Scripts.Components;
using DataContainers;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

public class PlayerStatusController : MonoBehaviour, IReceiveDamage, IRecoverHealth
{
    public PlayerData _data;
    private CharacterGUIController _characterGui;
    public Action OnFullHealth { get; }
    public Action OnRecoverHealth { get; }
    public Action OnDead { get; set; }
    public Action<float> OnReceiveDamage { get; }
    public IAnimatorService _animator;
    public bool dead { get; set; }
    [SerializeField] private NavMeshAgent _agent;

    private void Awake()
    {
        _characterGui = GetComponent<CharacterGUIController>();
        _animator = GetComponent<IAnimatorService>();
        _agent = GetComponent<NavMeshAgent>();

        //Assert.IsNotNull(data, "PlayerData nulo, verificar componente");
        Assert.IsNotNull(_characterGui, "CharacterGUI nulo, verificar componente");
    }

    private void Start()
    {
        //InitStats();
    }

    public void InitStats(PlayerData playerData)
    {
        dead = false;
        _data = playerData;
        _characterGui.SetLevelNumber(playerData.Level);
        _characterGui.SetExpSlider(playerData.currExperience);
        _characterGui.SetMaxSliderHealth(playerData.MaxHealth);
        _characterGui.SetSliderHealth(playerData.Health);
        SetAttackSpeed(0);
        if (_data.MovementSpeed < 5)
        {
            _data.MovementSpeed += 5;
//            _data.MovementSpeed = 5;
        }

        _agent.speed = _data.MovementSpeed;
    }


    public void ReduceHealth(float amount)
    {
        if (_data.Health > 0)
        {
            _data.Health -= amount;
            OnReceiveDamage?.Invoke(_data.Health);
//            Debug.Log("HEALTH -> " + _data.Health);
            // ReSharper disable once Unity.NoNullPropogation
            _characterGui?.SetUiHealth(_data.Health);
        }

        if (_data.Health <= 0 && !dead)
        {
            dead = true;
            OnDead?.Invoke();
        }
    }

    public void RecoverHealth(float amount)
    {
        _data.Health += amount;
        if (_data.Health > _data.MaxHealth)
        {
//            OnFullHealth?.Invoke();
            _data.Health = _data.MaxHealth;
        }

        OnRecoverHealth?.Invoke();
        if (_characterGui != null) _characterGui.SetSliderHealth(_data.Health);
    }

//    private void OnGUI()
//    {
//        if (GUI.Button(new Rect(60f, 60f, 100f, 50f), "Reduce Health"))
//        {
//            ReduceHealth(1);
//        }
//    }

    public Action OnPointsAdded;

    public void AddPoints(float points)
    {
        _data.points += points;
        OnPointsAdded?.Invoke();
    }

    public void AddExp(float exp)
    {
        _data.currExperience += exp;
        if (_data.currExperience > _data.maxExperience)
        {
            _data.currExperience = 0;
            LevelUp();
        }

        _characterGui.SetExpSlider(_data.currExperience);
    }

    private void SetAttackSpeed(float amount)
    {
        _data.Agility += amount;
        _animator.animator.SetFloat("AttackSpeed", _data.Agility);
    }

    private void AddMovementSpeed(float amount)
    {
        _data.MovementSpeed += amount;
        _agent.speed = _data.MovementSpeed;
        _animator.animator.SetFloat("MovementSpeed", (_data.MovementSpeed / 3));
    }

    public Action<int> OnLevelUp;

    private void LevelUp()
    {
        _data.MaxHealth += 23;
        _data.Health = _data.MaxHealth;
        _characterGui.SetMaxSliderHealth(_data.MaxHealth);
        _characterGui.SetSliderHealth(_data.Health);

        _data.Damage += 3;

        SetAttackSpeed(0.2f);
        AddMovementSpeed(0.1f);
        _data.Level += 1;
        _characterGui.SetLevelNumber(_data.Level);
        OnLevelUp?.Invoke(_data.Level);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            LevelUp();
        }
    }

//    private void OnGUI()
//    {
//        if (GUI.Button(new Rect(150, 50, 50, 50), "level"))
//        {
//            LevelUp();
//        }
//    }
}