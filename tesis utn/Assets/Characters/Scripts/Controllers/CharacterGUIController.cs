﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Characters
{
    public class CharacterGUIController : MonoBehaviour
    {
        [SerializeField] private Image _healthSlider;
        [SerializeField] private Image _expSlider;
        [SerializeField] private Text _levelNum;
        [SerializeField] private Text _currentHealth;
        [SerializeField] private Text _maxNumberHealth;
        private float maxHealth;

        [SerializeField] private bool lookToCamera = false;

        // Use this for initialization
        void Start()
        {
//			if(lookToCamera)
//			StartCoroutine(LookToCamera());
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (lookToCamera)
            {
                if (_healthSlider != null)
                    _healthSlider.transform.parent.LookAt(Camera.main.transform);
            }
        }

        public void SetSliderHealth(float amount)
        {
//            StartCoroutine(ShowSlider());
            if (_healthSlider != null)
            {
                SetUiHealth(amount);
            }
        }

        public void SetMaxSliderHealth(float maxHealthPoints)
        {
            this.maxHealth = maxHealthPoints;
//            if (_healthSlider != null)
//            {
//                _healthSlider.fillAmount = 1;
//            }

            if (_maxNumberHealth != null)
            {
                _maxNumberHealth.text = maxHealthPoints.ToString();
            }
        }

        public void ShowSlider(bool show)
        {
            this._healthSlider.transform.parent.gameObject.SetActive(show);
        }

        IEnumerator LookToCamera()
        {
            var cam = Camera.main;
            var parent = _healthSlider.transform.parent;

            while (lookToCamera)
            {
                parent.LookAt(cam.transform);
                yield return new WaitForSeconds(0.1f);
            }
        }

        private IEnumerator ShowSlider()
        {
            if (_healthSlider.transform.parent.gameObject.activeInHierarchy) yield break;
            _healthSlider.transform.parent.gameObject.SetActive(true);
            //yield return new WaitForSeconds(5);
            //_healthSlider.gameObject.SetActive(false);
        }

        public void SetExpSlider(float amount)
        {
            if (_expSlider != null)
                _expSlider.fillAmount = amount * 0.01f;
        }

        public void SetLevelNumber(int lvl)
        {
            if (_levelNum != null)
                _levelNum.text = lvl.ToString();
        }

        public void SetUiHealth(float amount)
        {
            if (amount < 0) amount = 0;
            var sliderAmount = amount / maxHealth;
            _healthSlider.fillAmount = sliderAmount;

            if (_currentHealth != null)
                _currentHealth.text = amount.ToString();
        }
        
        
    }
}