﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters.Scripts.Components;
using Enums;
using UnityEngine;

public class AIMovement : MonoBehaviour, IMoveable
{
    public Action<Vector3> OnNewPosition { get; set; }
    public Action<Vector3> OnPositionChanged { get; set; }
    
    
    public Action OnPositionReached { get; set; }
    public void StopPlayer()
    {
        throw new NotImplementedException();
    }

    public IObservable<long> MoveToInteract(GameObject enemy, float range)
    {
        throw new NotImplementedException();
    }


    [SerializeField] private Transform target;
    private IMoveable targetMoveable;


    private void Start()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
            
        }
    }

    private void Update()
    {
        OnNewPosition(target.transform.position);
    }
//    private IEnumerator OnChasePlayer()
//    {
//        if (state == EnemyState.Chase) yield break;
//        state = EnemyState.Chase;
//        nav.isStopped = false;
//        Vector3 playerPos = Vector3.zero;
////        var distance = Vector3.Distance(transform.position,player.transform.position);
//        while (state == EnemyState.Chase)
//        {
//            var distance = Vector3.Distance(transform.position, player.transform.position);
//            if (playerPos != player.transform.position)
//            {
//                playerPos = player.transform.position;
//                this.nav.SetDestination(playerPos);
//            }
//
//            if (distance <= data.AttackRange)
//            {
//                Debug.LogWarning("RANGO DE ATAQUE");
//                this.nav.isStopped = true;
//                StartCoroutine(AttackPlayer());
//                yield break;
//            }
//
//            yield return new WaitForSeconds(.5f);
//        }
//    }    
}