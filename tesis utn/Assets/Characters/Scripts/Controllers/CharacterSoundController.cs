﻿using Characters.Scripts.Components;
using UnityEngine;
using UnityEngine.Serialization;

public class CharacterSoundController : MonoBehaviour
{
    private IAnimatorService _animator;
    private CharacterMecanimBehaviour _behaviour;

    [SerializeField] private AudioClip _leftFootClip;
    [SerializeField] private AudioClip _rightFootClip;
    [SerializeField] private AudioClip[] _attack;
    [SerializeField] private AudioClip[] _hurt;
    [SerializeField] private AudioSource _audioSource;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<IAnimatorService>();
        if (_animator != null)
        {
            _behaviour = _animator.ReturnMachineBehaviour() as CharacterMecanimBehaviour;
            if (_behaviour != null)
            {
                _behaviour.onLeftFoot += delegate { PlayClip(_leftFootClip); };
                _behaviour.onRightFoot += delegate { PlayClip(_rightFootClip); };
                _behaviour.OnStartAttack += delegate { PlayRandomClips(_attack); };
                _behaviour.OnInjured += delegate { PlayRandomClips(_hurt); };
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }


    void PlayClip(AudioClip clip)
    {
        if (clip == null) return;
        _audioSource.clip = clip;
        _audioSource.Play();
    }

    void PlayRandomClips(AudioClip[] clips)
    {
        if (clips.Length == 0) return;
        var randomNumber = Random.Range(0, clips.Length);
        var clip = clips[randomNumber];
        _audioSource.clip = clip;
        _audioSource.Play();
    }
}