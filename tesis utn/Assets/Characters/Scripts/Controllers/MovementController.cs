﻿using System;
using System.Net;
using System.Runtime.InteropServices;
using Characters.Scripts.Components;
using UniRx;
using Unity.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class MovementController : MonoBehaviour, IMoveable
{
    [SerializeField] public NavMeshAgent nav;

    private IInputStream _input;
    private IAnimatorService _animator;
    private IObservable<long> _posReachedObs;
    private IDisposable _posReachedSubscription;
    private IReceiveDamage _receiveDamage;


    public Action<Vector3> OnNewPosition { get; set; }
    public Action<Vector3> OnPositionChanged { get; set; }
    public Action OnPositionReached { get; set; }
    public bool deadPlayer = false;
    public bool stopWhenAttack = true;
    // Use this for initialization

    private void Awake()
    {
        _animator = GetComponent<IAnimatorService>();
        _input = GetComponent<IInputStream>();
        nav = GetComponent<NavMeshAgent>();
        _receiveDamage = GetComponent<IReceiveDamage>();
    }

    void Start()
    {
        _input.OnNewPosition += MoveCharacter;
        //_input.OnAttackButton += OnAttackButton;
        _input.OnInteractButton += OnInteractButton;
        _receiveDamage.OnDead += OnDead;
        //me suscribo a una funcion que se dispara cuando el personaje llega posicion de destino.
        _posReachedObs = Observable.EveryFixedUpdate().Where(_ => !deadPlayer &&
            Math.Abs(nav.remainingDistance) < 0.1f);

        GameManager.Instance.OnInitMenu += OnInitMenu;
        GameManager.Instance.OnInitGame += OnInitGame;
        GameManager.Instance.OnGameEnd += OnGameEnd;
    }

    private void OnGameEnd()
    {
        _animator.PlayAnimation("onRun", false);
        _posReachedSubscription?.Dispose();
        nav.isStopped = true;
        nav.ResetPath();
        
//        GameManager.Instance.OnInitGame -= OnInitGame;
    }

    private void OnInitGame()
    {
        deadPlayer = false;
        _animator.animator.Play("idle", 0);
//        Animator.Play(state, layer, normalizedTime);    
    }

    private void OnDead()
    {
        deadPlayer = true;
        _posReachedSubscription?.Dispose();
        _animator.SetTriggerAnimation("onDie");
    }

    private void OnInitMenu()
    {
        if (nav != null)
            nav.isStopped = true;
        
        GameManager.Instance.OnInitGame += OnInitGame;
        _animator.PlayAnimation("onRun", false);
    }

    private void OnInteractButton(GameObject obj, Vector3 pos)
    {
        //        transform.LookAt(pos);
    }

    public void StopPlayer()
    {
        nav.isStopped = true;
        nav.velocity = Vector3.zero;
        _animator.PlayAnimation("onRun", false);
    }

    public IObservable<long> MoveToInteract(GameObject interactable, float range)
    {
        OnPositionReached?.Invoke();
        var targetPosition = interactable.transform.position;
        MoveCharacter(targetPosition);
        return Observable.EveryFixedUpdate()
            .Where(_ =>
            {
                var heading = targetPosition - transform.position;
                var distance = heading.magnitude;
                return distance < (range - 0.1f);
            });
    }

    private void OnAttackButton(GameObject obj)
    {
        _animator.PlayAnimation("onRun", false);
        nav.isStopped = true;
    }

    private void MoveCharacter(Vector3 pos)
    {
        if (_animator.GetAnimationState("onAttack") && stopWhenAttack)
            return;
        //todo: ver de reiniciar animator luego de setear el trigger de morir
        //todo: solo para test de los enemigos inmortales. 

        _animator.PlayAnimation("onAttack", false);
        if (!_animator.GetAnimationState("onRun"))
        {
            nav.isStopped = false;
            _animator.PlayAnimation("onRun", true);

            //Evita las multiples subscripciones,
            //Al llegar a la posicion, detiene la animacion de movimiento.
            _posReachedSubscription?.Dispose();
            _posReachedSubscription = _posReachedObs
                .Subscribe(_ =>
                {
                    _animator.PlayAnimation("onRun", false);
                    OnPositionReached?.Invoke();
                });
        }

        this.nav.SetDestination(pos);
    }
    
    private void OnDestroy()
    {
        GameManager.Instance.OnInitGame -= OnInitGame;
        GameManager.Instance.OnGameEnd -= OnGameEnd;
    }
}