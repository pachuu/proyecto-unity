﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

public class CharacterMecanimBehaviour : StateMachineBehaviour
{
    public Action OnAttackAnimFinish;
    public Action OnStartAttack;
    public Action OnAttackedAnimation;

    [Range(0, 1f)] [SerializeField] private float _timeToStartAttackClip = 0.1f;

    [Range(0, 1f)] [SerializeField] private float _timeToAttack = 0.6f;
//    public bool attackEmited = false;

    private int time = 0;

    public Action onLeftFoot;

    public Action onRightFoot;
    public Action OnInjured;

    [FormerlySerializedAs("firstAttaqued")] public bool firstAttacked = false;

    //OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
//        if (stateInfo.IsName("atk01"))
//            OnStartAttack?.Invoke();

        if (stateInfo.IsName("injured"))
        {
            OnInjured?.Invoke();
        }
    }

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName("atk01"))
        {
            PlayAttackSound(stateInfo);
        }

        if (stateInfo.IsName("run"))
        {
            PlayFootstepsSound(stateInfo);
        }
    }

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("---> OnStateExit" + stateInfo.nameHash + " <> layer Index :" + layerIndex);
        if (stateInfo.IsName("atk01"))
        {
            OnAttackAnimFinish?.Invoke();
        }
    }

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMachineEnter is called when entering a statemachine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash){
    //
    //}

    // OnStateMachineExit is called when exiting a statemachine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash) {
    //
    //}

    private void PlayAttackSound(AnimatorStateInfo stateInfo)
    {
        time = (int) stateInfo.normalizedTime;
        if (Math.Abs(stateInfo.normalizedTime - (_timeToStartAttackClip + time)) < (0.01))
            OnStartAttack?.Invoke();

        // Debug.Log("attaking -> current progress -> " + stateInfo.normalizedTime);
        if (stateInfo.normalizedTime > (_timeToAttack + time) && !firstAttacked)
        {
            firstAttacked = true;
            OnAttackedAnimation?.Invoke();
        }
        else if (stateInfo.normalizedTime <= (_timeToAttack + time))
        {
            firstAttacked = false;
        }
    }

    private void PlayFootstepsSound(AnimatorStateInfo stateInfo)
    {
        time = (int) stateInfo.normalizedTime;
        if (Math.Abs(stateInfo.normalizedTime - (time + 0.1)) < 0.01f)
        {
            onLeftFoot?.Invoke();
//            Debug.Log("pie izquierdo");
        }

        if (Math.Abs(stateInfo.normalizedTime - (time + 0.63)) < 0.01f)
        {
            onRightFoot?.Invoke();
//            Debug.Log("pie derecho");
        }
    }

    public void SetTimeToTriggerAttack(float time)
    {
        _timeToAttack = time;
    }
}