﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using Enums;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    [SerializeField] private GameObject _canvasInGame;
    [SerializeField] private GameObject _canvasMenu;
    [SerializeField] private GameObject _panelGameOver;

    [SerializeField] private Button _playButton;
    [SerializeField] private Button _continueButton;
    [SerializeField] private Button _optionsButton;

    [SerializeField] private Button _exitButton;

    private void Awake()
    {
        GameManager.Instance.OnInitMenu += OnInitMenu;
        GameManager.Instance.OnInitGame += OnInitGame;
        GameManager.Instance.OnOldGameFound += OnOldGameFound;
        GameManager.Instance.OnGameEnd += OnGameEnd;
    }

    private void OnGameEnd()
    {
//        _continueButton.interactable = false;
        StartCoroutine(ShowGameOverScreen());
        OnInitMenu();
    }

    private IEnumerator ShowGameOverScreen()
    {
        yield return new WaitForSeconds(5);
        _continueButton.gameObject.SetActive(false);
        _panelGameOver.SetActive(true);
        _canvasMenu.SetActive(true);
    }


    private void OnOldGameFound()
    {
        _continueButton.gameObject.SetActive(true);
    }

    // Use this for initialization
    void Start()
    {
        _playButton.onClick.AddListener(OnPlayButton);
        _continueButton.onClick.AddListener(OnContinueButton);
        _exitButton.onClick.AddListener(OnExitButton);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.GameMode == GameState.Game)
        {
            if (!GameManager.Instance.OnPause)
            {
                GameManager.Instance.SavePlayerData();
                ShowMenu();
                GameManager.Instance.OnPause = true;
                Time.timeScale = 0.05f;
            }
            else
            {
                Time.timeScale = 1;
                OnInitGame();
                GameManager.Instance.OnPause = false;
            }
        }
    }


    private void OnExitButton()
    {
        if (GameManager.Instance.OnPause)
        {
            GameManager.Instance.SavePlayerData();
            GameManager.Instance.InitMenu();
        }
        else
        {
            Application.Quit();
        }
    }

    private void OnContinueButton()
    {
        GameManager.Instance.ContinueGame();
    }

    private void OnPlayButton()
    {
        if (GameManager.Instance.GameMode != GameState.Game)
        {
            GameManager.Instance.StartNewGame();
            _continueButton.gameObject.SetActive(false);
        }
        else
        {
            GameManager.Instance.OnResumeButton();
            OnInitGame();
        }     
    }

    private void OnInitMenu()
    {
        _panelGameOver.SetActive(false);
        _continueButton.interactable = true;
        _canvasInGame.SetActive(false);
        _playButton.GetComponentInChildren<Text>().text = "Nueva Partida";
        _exitButton.GetComponentInChildren<Text>().text = "Salir";
    }

    private void OnInitGame()
    {
        _continueButton.gameObject.SetActive(false);
        _canvasMenu.SetActive(false);
        _canvasInGame.SetActive(true);
        _playButton.GetComponentInChildren<Text>().text = "Reanudar";
        _exitButton.GetComponentInChildren<Text>().text = "Volver al menú";
    }

    private void ShowMenu()
    {
        _canvasMenu.SetActive(true);
        _canvasInGame.SetActive(false);
    }
}