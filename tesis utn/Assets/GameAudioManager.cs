﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class GameAudioManager : MonoBehaviour
{
    [Range(0.0f, 1.0f)] [SerializeField] private float _maxVolMusic;
    public AudioMixerGroup MusicOutputAudioMixer;

    [Header("Short cues")] 
    
    [SerializeField] private AudioClip _audioClipLevelUp;

    [Header("Music cues")] 
    
    [SerializeField] private AudioClip[] _audioClipsMenu;
    [SerializeField] private AudioClip[] _audioClipsNight;
    [SerializeField] private AudioClip[] _audioClipsDay;
    [SerializeField] private AudioClip[] _audioClipsEndGame;

    [Header("Audio sources")] 
    
    private AudioSource[] _audioSource;
    [SerializeField] private AudioSource _cuesAudioSource;

    private IEnumerator[] _fader = new IEnumerator[2];
    private int _activePlayer = 0;

    private GameObject _player;


    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");

        //Generate the two AudioSources
        _audioSource = new AudioSource[2]
        {
            gameObject.AddComponent<AudioSource>(),
            gameObject.AddComponent<AudioSource>()
        };

        //Set default values
        foreach (AudioSource s in _audioSource)
        {
            s.loop = true;
            s.playOnAwake = false;
            s.volume = 0.0f;
            s.outputAudioMixerGroup = MusicOutputAudioMixer;
        }

        GameManager.Instance.OnInitMenu += OnInitMenu;
        GameManager.Instance.OnInitGame += PlayDayMusic;
        GameManager.Instance.OnGameEnd += PlayEndGameMusic;
    }

    void Start()
    {
        SpawnManager.Instance.OnSpawnWaveStart += PlayNightMusic;
        SpawnManager.Instance.OnSpawnWaveFinish += PlayDayMusic;

        _player.GetComponent<PlayerStatusController>().OnLevelUp += PlayLevelUpMusic;
    }

    public void PlayNightMusic()
    {
        var random = Random.Range(0, _audioClipsNight.Length);
        FadeMusic(_audioClipsNight[random]);
    }

    private void PlayDayMusic()
    {
        var random = Random.Range(0, _audioClipsDay.Length);
        FadeMusic(_audioClipsDay[random]);
    }

    private void PlayEndGameMusic()
    {
        foreach (AudioSource s in _audioSource)
        {
            s.volume = 0.0f;
        }

        DayCicleController.Instance.DayRain();

        var random = Random.Range(0, _audioClipsEndGame.Length);
        _cuesAudioSource.priority = 0;
        _cuesAudioSource.clip = _audioClipsEndGame[random];
        _cuesAudioSource.Play();
    }

    private void OnInitMenu()
    {
        var random = Random.Range(0, _audioClipsMenu.Length);
        FadeMusic(_audioClipsMenu[random]);
    }

    private void PlayLevelUpMusic(int obj)
    {
        _cuesAudioSource.clip = _audioClipLevelUp;
        _cuesAudioSource.Play();
    }

    void FadeMusic(AudioClip audioClip)
    {
        if (audioClip == _audioSource[_activePlayer].clip)
        {
            return;
        }

        foreach (IEnumerator i in _fader)
        {
            if (i != null)
            {
                StopCoroutine(i);
            }
        }

        if (_audioSource[_activePlayer].volume > 0)
        {
            _fader[0] = FadeMusicOut(_audioSource[_activePlayer]);
            StartCoroutine(_fader[0]);
        }

        //Fade-in the new clip
        int nextPlayer = (_activePlayer + 1) % _audioSource.Length;
        _audioSource[nextPlayer].clip = audioClip;
        _audioSource[nextPlayer].Play();
        _fader[1] = FadeMusicIn(_audioSource[nextPlayer]);
        StartCoroutine(_fader[1]);

        //Register new active player
        _activePlayer = nextPlayer;
    }

    IEnumerator FadeMusicIn(AudioSource audioSource)
    {
        audioSource.volume = 0F;

        while (audioSource.volume < _maxVolMusic - 0.01F)
        {
            audioSource.volume = Mathf.Lerp(audioSource.volume, _maxVolMusic, Time.deltaTime / 3);
            yield return 0;
        }

        audioSource.volume = _maxVolMusic;
    }

    IEnumerator FadeMusicOut(AudioSource audioSource)
    {
        audioSource.volume = _maxVolMusic;

        while (audioSource.volume > 0.01F)
        {
            audioSource.volume = Mathf.Lerp(audioSource.volume, 0F, Time.deltaTime / 2);
            yield return 0;
        }

        audioSource.volume = 0F;
    }
}