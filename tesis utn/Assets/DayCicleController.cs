﻿using System;
using System.Collections;
using DigitalRuby.RainMaker;
using UnityEngine;

public class DayCicleController : MonoBehaviour
{
    public static DayCicleController Instance { get; private set; }
    [SerializeField] private Light _gameLight;
    [SerializeField] private RainScript _rain;

    public Action OnNight;
    public Action OnDay;

    private Coroutine _dayLightCoroutine;

    private void Awake()
    {
        //Check if instance already exists
        if (Instance == null)

            //if not, set instance to this
            Instance = this;

        //If instance already exists and it's not this:
        else if (Instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one
            //instance of a GameManager.
            Destroy(gameObject);
    }

    // Use this for initialization
    private void Start()
    {
        GameManager.Instance.OnInitMenu += OnInitMenu;
        GameManager.Instance.OnInitGame += OnInitGame;
        GameManager.Instance.OnGameEnd += OnGameEnd;

        SpawnManager.Instance.OnSpawnWaveFinish += MakeDay;
        RenderSettings.ambientLight = Color.black;
    }

    private void OnGameEnd()
    {
        StopCoroutine(_dayLightCoroutine);
    }

    private void OnInitGame()
    {
        Color color;
        if (ColorUtility.TryParseHtmlString("#717D9A", out color))
        {
            RenderSettings.ambientLight = color;
        }

        MakeDay();
    }

    private void OnInitMenu()
    {
        if (_dayLightCoroutine != null)
        {
            StopCoroutine(_dayLightCoroutine);
        }

        RenderSettings.ambientLight = Color.black;
        _rain.RainIntensity = 0.15f;
        _gameLight.intensity = 0;
    }

    private void MakeDay()
    {
        var currentRotation = _gameLight.transform.rotation;
//        _gameLight.transform.rotation = new Quaternion(0, currentRotation.y, currentRotation.z, currentRotation.w);
        _gameLight.transform.rotation = Quaternion.Euler(0, currentRotation.y, currentRotation.z);
        DayRain();
        _dayLightCoroutine = StartCoroutine(CicleDay());
    }

    public void DayRain()
    {
        _rain.RainIntensity = 0.15f;
    }

    private void MakeNight()
    {
        if (_dayLightCoroutine != null)
            StopCoroutine(_dayLightCoroutine);

        _gameLight.intensity = 0;
        StartCoroutine(NightRain());
    }

    private IEnumerator NightRain()
    {
        while (_rain.RainIntensity < 0.7f)
        {
            _rain.RainIntensity += 0.005f;
            yield return new WaitForSeconds(0.1f);
        }

        OnNight?.Invoke();
    }

    IEnumerator CicleDay()
    {
        var lightTransform = _gameLight.transform;
        _gameLight.intensity = 0;
        OnDay?.Invoke();
        while (lightTransform.rotation.eulerAngles.x < 200)
        {
            if (lightTransform.rotation.eulerAngles.y <= 180)
            {
                if (_gameLight.intensity < 1.15f)
                    _gameLight.intensity += 0.01f;

                if (_rain.RainIntensity > 0.15)
                {
                    _rain.RainIntensity -= 0.005f;
                }
            }
            else if (lightTransform.rotation.eulerAngles.y >= 180)
            {
                if (_gameLight.intensity > 0)
                    _gameLight.intensity -= 0.005f;
            }


            lightTransform.Rotate(0.5f, 0, 0);
            yield return new WaitForSeconds(0.05f);
        }

        MakeNight();
    }


//    private void OnGUI()
//    {
//        if (GUI.Button(new Rect(50, 50, 150, 50), "Day"))
//        {
//            MakeDay();
//        }
//
//        if (GUI.Button(new Rect(50, 100, 150, 50), "Night"))
//        {
//            MakeNight();
//        }
//    }
}