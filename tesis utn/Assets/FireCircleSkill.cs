﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class FireCircleSkill : MonoBehaviour, ISkill
{
    public PlayerStatusController _PlayerStatus { get; set; }
    public int amount { get; set; }
    private Coroutine _damageCoroutine;

    private void OnEnable()
    {
        _damageCoroutine = StartCoroutine(AreaDamage());
    }

    private IEnumerator AreaDamage()
    {
        var layer = 1 << 10;
        while (true)
        {
            var enemiesAround = Physics.OverlapSphere(_PlayerStatus.transform.position, 3, layer);
            foreach (var e in enemiesAround)
            {
                if (e.gameObject.tag.Equals("Enemy"))
                {
                    var enemyStatus = e.gameObject.GetComponent<EnemyStatusController>();
                    enemyStatus?.ReduceHealth(amount);
                }
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    private void OnDisable()
    {
        StopCoroutine(_damageCoroutine);
    }

    public void InitSkill(PlayerStatusController _player, int amount)
    {
        this._PlayerStatus = _player;
        this.amount = amount;
        gameObject.SetActive(true);
    }

    public void StopSkill()
    {
        gameObject.SetActive(false);
    }
}