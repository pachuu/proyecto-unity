﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSkill : MonoBehaviour, ISkill {

	public PlayerStatusController _PlayerStatus { get; set; }
	public int amount { get; set; }

	public void InitSkill(PlayerStatusController _player, int amount)
	{
		_PlayerStatus = _player;
		this.amount = amount;
		gameObject.SetActive(true);
		_player.RecoverHealth(amount);
	}

	public void StopSkill()
	{
		gameObject.SetActive(false);
	}
}
