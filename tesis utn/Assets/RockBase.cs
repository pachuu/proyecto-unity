﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters.Scripts.Components;
using UnityEngine;
using UnityEngine.UI;

public class RockBase : MonoBehaviour, IReceiveDamage
{
    [SerializeField] private Image _healthSlider;
    [SerializeField] private Text _healtText;
    [SerializeField] private Light _light;
    private Outline _outline;
    private Color _originalColor;
    private Coroutine OnDamageColor;

    public float Health { get; private set; }
    public float MaxHealth;

    private PlayerStatusController _playerStatus;

    // Use this for initialization

    private void Awake()
    {
        MaxHealth = 1000;
        _outline = GetComponent<Outline>();
        _playerStatus = FindObjectOfType<PlayerStatusController>();
    }

    void Start()
    {
        _originalColor = _light.color;
        GameManager.Instance.OnInitGame += () => { _light.color = _originalColor; };
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ReduceHealth(600);
        }
    }

    public void SetHealth(float amount)
    {
        Health = amount;
        SetUiHealth(Health);
    }

    private void SetUiHealth(float amount)
    {
        if (amount < 0) amount = 0;
        float sliderAmount = amount / MaxHealth;
        _healthSlider.fillAmount = sliderAmount;
        var textAmount = 100 * amount / MaxHealth;
        _healtText.text = textAmount.ToString();
    }

    public Action OnDead { get; set; }
    public Action<float> OnReceiveDamage { get; }

    public void ReduceHealth(float amount)
    {
       
        if (Health > 0)
        {
            Health -= amount;
            SetUiHealth(Health);
            Debug.Log("Base receive damage");
            if (OnDamageColor != null)
                StopCoroutine(OnDamageColor);

            OnDamageColor = StartCoroutine(DamageColor());
        }

        if (Health <= 0)
        {
            OnDead?.Invoke();
//            GameManager.Instance.EndGame();
            Debug.Log("Game Over");
            StopCoroutine(OnDamageColor);
            _light.color = Color.red;
            _playerStatus.dead = true;
            _playerStatus.OnDead?.Invoke();
        }
    }

    public bool dead { get; set; }

    private IEnumerator DamageColor()
    {
        _light.color = Color.red;
        yield return new WaitForSeconds(1);
        _light.color = _originalColor;
    }

    private void OnMouseEnter()
    {
        _outline.enabled = true;
    }

    private void OnMouseExit()
    {
        _outline.enabled = false;
    }
}