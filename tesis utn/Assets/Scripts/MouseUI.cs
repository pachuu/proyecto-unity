﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Characters.Scripts.Components;
using UnityEngine;

public class MouseUI : MonoBehaviour
{
    // Use this for initialization

    [SerializeField] private Texture2D[] _cursors;

    //singleton
    public static MouseUI Instance = null;
    [SerializeField] private MouseInput _mouseInput;
    [SerializeField] private IMoveable _playerMoveable;
    [SerializeField] private GameObject pointerPrefab;
    private GameObject pointerToWalk;

    private void Awake()
    {
        //Check if instance already exists
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        _playerMoveable = _mouseInput.GetComponent<IMoveable>();
    }

    private void Start()
    {
        _mouseInput.OnNewPosition += ShowClickedPosition;
        _playerMoveable.OnPositionReached += RecyclePointerPrefab;
        _mouseInput.OnInteractButton += RecyclePointerPrefab;
        GameManager.Instance.OnGameEnd += OnGameEnd;
    }

    private void OnGameEnd()
    {
         pointerToWalk.gameObject.SetActive(false);
    }

    private void RecyclePointerPrefab(GameObject arg1, Vector3 arg2)
    {
        if (pointerToWalk != null)
        {
            pointerToWalk.gameObject.SetActive(false);
        }
    }


    private void ShowClickedPosition(Vector3 pos)
    {
        if (pointerToWalk == null)
        {
            pointerToWalk = Instantiate(pointerPrefab, Vector3.zero, Quaternion.identity);
        }

        pointerToWalk.transform.position = pos;
        pointerToWalk.gameObject.SetActive(true);
    }

    private void RecyclePointerPrefab()
    {
        if (pointerToWalk != null)
        {
            pointerToWalk.gameObject.SetActive(false);
        }
    }

    public void ChangeCursor(CursorType cursorType)
    {
        try
        {
            var cursor = _cursors.DefaultIfEmpty(_cursors[0])
                .SingleOrDefault(x => x.name == cursorType.ToString());

            Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
        }
        catch (Exception e)
        {
            Debug.LogError("Cursor no encontrado");
        }
    }
}


public enum CursorType
{
    normal = 0,
    attack = 1
}