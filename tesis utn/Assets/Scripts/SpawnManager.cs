﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters.Scripts;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance { get; private set; }

    [Header("Spawn and wave settings")] [SerializeField]
    private Text _currentWave;

    [SerializeField] private Transform[] _spawnPoint;

    //Wave controls
    [SerializeField] private bool _generateEnemies = true;

    public int TotalWaves = 5;
    private int _enemiesWaves;

    public int _numWaves { get; private set; }

    public Action OnSpawnWaveStart;
    public Action OnSpawnWaveFinish;

    // The different Enemy levels
    private enum EnemyLevels
    {
        Melee,
        Ranged,
        Tank,
        Boss
    }

    private EnemyLevels _enemyLevel;

    [Header("Enemies")] public GameObject MeleeEnemy;
    public GameObject RangedEnemy;
    public GameObject TankEnemy;
    public GameObject BossEnemy;

    private readonly Dictionary<EnemyLevels, GameObject> _enemies = new Dictionary<EnemyLevels, GameObject>(4);
    private List<GameObject> _spawnedEnemies;

    // Enemy level to be spawnedEnemy

    // Enemies and how many have been created and how many are to be created
    //----------------------------------

    [Header("Enemies Numbers")] [SerializeField]
    private Transform _enemiesPool;

    public int TotalEnemies = 10;
    public int NumberOfEnemiesIncrement = 1;
    private int _numEnemy;
    private int _numMelee;
    private int _numRanged;

    private Coroutine spawnCoroutine;

    //----------------------------------
    // End of Enemy Settings
    //-------

    private void Awake()
    {
        //Check if instance already exists
        if (Instance == null)

            //if not, set instance to this
            Instance = this;

        //If instance already exists and it's not this:
        else if (Instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one
            //instance of a GameManager.
            Destroy(gameObject);
    }

    private void Start()
    {
        if (BossEnemy != null)
        {
            _enemies.Add(EnemyLevels.Boss, BossEnemy);
        }

        if (MeleeEnemy != null)
        {
            _enemies.Add(EnemyLevels.Melee, MeleeEnemy);
        }

        if (RangedEnemy != null)
        {
            _enemies.Add(EnemyLevels.Ranged, RangedEnemy);
        }

        if (TankEnemy != null)
        {
            _enemies.Add(EnemyLevels.Tank, TankEnemy);
        }

        if (_generateEnemies)
        {
            DayCicleController.Instance.OnNight += StartWave;
        }

        _spawnedEnemies = new List<GameObject>();

        GameManager.Instance.OnGameEnd += ResetWaves;
    }

    private void ResetWaves()
    {
        _numEnemy = 0;
        _numWaves = 0;

        if (spawnCoroutine != null)
            StopCoroutine(spawnCoroutine);

        StartCoroutine(ClearEnemiesGameObjects());
    }

    private IEnumerator ClearEnemiesGameObjects()
    {
        yield return new WaitForSeconds(5);
        foreach (var e in _spawnedEnemies)
        {
            Destroy(e.gameObject);
        }

        _spawnedEnemies.Clear();
    }

    public void StartWave()
    {
        OnSpawnWaveStart?.Invoke();

        _numWaves++;
        _currentWave.text = _numWaves.ToString();

        // Reset enemies
        _numMelee = 0;
        _numRanged = 0;
        _enemiesWaves = BossEnemy ? TotalWaves - 1 : TotalWaves;

        spawnCoroutine = StartCoroutine(EnemyWave());
    }

    IEnumerator EnemyWave()
    {
        while (_numEnemy < TotalEnemies && _numWaves <= TotalWaves)
        {
            if (_numWaves <= _enemiesWaves)
            {
                if (_numMelee > TotalEnemies / _numWaves && _numRanged <= _numMelee &&
                    RangedEnemy != null)
                {
                    _enemyLevel = EnemyLevels.Ranged;
                    _numRanged++;
                }
                else if (MeleeEnemy != null)
                {
                    _enemyLevel = EnemyLevels.Melee;
                    _numMelee++;
                }
            }
            else if (BossEnemy != null)
            {
                _enemyLevel = EnemyLevels.Boss;
                TotalEnemies = 1;
            }

            //spawns an enemy
            SpawnEnemy();

            var randomSpawnTime = Random.Range(0.1f, 1.3f);
            yield return new WaitForSeconds(randomSpawnTime);
        }
    }

    private void SpawnEnemy()
    {
        var enemy = Instantiate(_enemies[_enemyLevel], _spawnPoint[Random.Range(0, _spawnPoint.Length)].position,
            Quaternion.identity);
        _spawnedEnemies.Add(enemy);
        enemy.transform.parent = _enemiesPool;

        // Increase the total number of enemies spawned
        _numEnemy++;
    }

    // Call this function from the enemy when it "dies" to remove an enemy count
    public void KillEnemy(GameObject e)
    {
        _numEnemy--;
        _spawnedEnemies.Remove(e);
        CheckDeaths();
    }

    private void CheckDeaths()
    {
        if (_numEnemy != 0) return;

        StopCoroutine(EnemyWave());

        //increase the number of enemies
        TotalEnemies += _numWaves != 0 ? NumberOfEnemiesIncrement : 0;

        // Reset enemies
        _numMelee = 0;
        _numRanged = 0;
        Debug.Log("<color=green> WAVE FINISHED, ALL ENEMY KILLED </color>");
        OnSpawnWaveFinish?.Invoke();
    }

    public void SetWave(int wave)
    {
        _numWaves = wave;
        _currentWave.text = wave.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            var enemyLevel = _enemyLevel;
            _enemyLevel = EnemyLevels.Boss;
            SpawnEnemy();
            _enemyLevel = enemyLevel;
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            var enemyLevel = _enemyLevel;
            _enemyLevel = EnemyLevels.Ranged;
            SpawnEnemy();
            _enemyLevel = enemyLevel;
        }
    }
//
//    private void OnGUI()
//    {
//        if (GUI.Button(new Rect(50, 50, 50, 50), "BOSS"))
//        {
//            var enemyLevel = _enemyLevel;
//            _enemyLevel = EnemyLevels.Boss;
//            SpawnEnemy();
//            _enemyLevel = enemyLevel;
//        }
//    }
}