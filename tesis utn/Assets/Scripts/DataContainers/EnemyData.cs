﻿using Enums;
using UnityEngine;

namespace DataContainers
{
    public class EnemyData : CharacterData
    {
        [Header("Datos específicos del Enemigo")]
        
        public EnemyType Type;
        public string EnemyName;
        public string Description;
        public float VisionRange;
        public int givenExperience;
    }
}