﻿namespace DataContainers
{
    [System.Serializable]
    public class TemporalDataWrapper
    {
        public int Health;
        public int MaxHealth;
        public int Energy;
        public int MaxEnergy;
        public int Damage;
        public float MovementSpeed;
        public float Agility;
        public int Level;
        public float AttackRange;
        
        public string nickname;
        public float score;
        public float currExperience;
        public float maxExperience;
        public int points;
    }
}