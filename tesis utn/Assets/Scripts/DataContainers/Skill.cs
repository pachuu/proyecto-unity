﻿using UnityEngine;
using UnityEngine.UI;

namespace DataContainers
{
    public class Skill
    {
        public string skillName;
        public int cooldownTime;
        public int maxCooldownTime;
        public Button viewButton;
        public GameObject SkillGameObjectView;
        public int lvlToUnblock;
        public int amount;
        public int activeTime;
        public bool unlocked;
        public bool active;
        public bool isPassive;
        public int passiveDuration;
        public ISkill _skillController;

        private Text text;
        private string letter { get; set; }

        public void SetTextCounter(int amount)
        {
            if (text == null)
            {
                text = viewButton.GetComponentInChildren<Text>();
                letter = text.text;
            }

            text.text = (amount != 0) ? amount.ToString() : letter;
        }

        public void SetSkillController()
        {
            _skillController = SkillGameObjectView.GetComponent<ISkill>();
        }

        public ISkill GetController()
        {
            return _skillController;
        }
    }
}