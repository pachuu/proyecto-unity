﻿namespace DataContainers
{
    [System.Serializable]
    public class GameStatusData
    {
        public float baseHealth;
        public int currentWave;
        public PlayerData player;
    }
}