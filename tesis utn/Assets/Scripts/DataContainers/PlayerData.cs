﻿using DataContainers;
using Enums;
using UnityEngine;

[System.Serializable]
public class PlayerData : CharacterData
{
    public string nickname;
    public float score;
    public float currExperience;
    public float maxExperience;
    public float points;
}    