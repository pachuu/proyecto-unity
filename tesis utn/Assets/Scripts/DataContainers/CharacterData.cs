﻿using Characters.Scripts.Components;
using UnityEngine;

namespace DataContainers
{
    [CreateAssetMenu]
    public class CharacterData : ScriptableObject
    {
        public float Health;
        public int MaxHealth;
        public int Energy;
        public int MaxEnergy;
        public int Damage;
        public float MovementSpeed;
        public float Agility;
        public int Level;
        public float AttackRange;
    }
}