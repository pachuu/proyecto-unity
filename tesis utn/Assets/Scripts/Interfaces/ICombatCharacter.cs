﻿using System.Collections;
using System.Collections.Generic;
using DataContainers;
using UnityEngine;

public interface ICombatCharacter  {

    void AttackTarget(CharacterData obj);
}
