﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovableCharacter  {

    void MoveToPoint(Vector3 point);
    void MovingCharacter();
}
