﻿using System.Collections;
using System.Collections.Generic;
using Characters;
using Characters.Scripts.Components;
using Enums;
using EZCameraShake;
using UnityEngine;

public class SimpleCameraController : MonoBehaviour
{
    // Use this for initialization
    private Transform _player;
    [SerializeField] [Range(-30, 30)] private float _offsetX, _offsetY, _offsetZ;
    [SerializeField] private CharacterAnimatorController _animatorService;
    private CharacterMecanimBehaviour _behaviour;
    [SerializeField] private Transform _menuCameraPosition;
    private bool _gameEnded;

    void Start()
    {
        GameManager.Instance.OnInitMenu += OnInitMenu;
        GameManager.Instance.OnInitGame += OnInitGame;
        GameManager.Instance.OnGameEnd += () => { _gameEnded = true; };
        this._player = GameObject.FindGameObjectWithTag("Player").transform;
        _animatorService = this._player.GetComponent<CharacterAnimatorController>();

        _behaviour = _animatorService.ReturnMachineBehaviour() as CharacterMecanimBehaviour;

        if (_behaviour != null)
        {
//            _behaviour.OnAttackAnimFinish += OnAttackAnimFinish;
            _behaviour.OnAttackedAnimation += OnAttackedAnimation;
        }
    }

    private void OnInitGame()
    {
        _gameEnded = false;
    }

    private void OnInitMenu()
    {
        transform.position = _menuCameraPosition.position;
        transform.rotation = _menuCameraPosition.rotation;
    }

    private void LateUpdate()
    {
        if (!_gameEnded)
            FollowCamera();
//        if (Input.GetKeyDown(KeyCode.R))
//        {
//            OnAttackedAnimation();
//        }
    }

    private void FollowCamera()
    {
        if (GameManager.Instance.GameMode == GameState.Game)
        {
            transform.LookAt(this._player);
            transform.position = _player.transform.position + new Vector3(_offsetX, _offsetY, _offsetZ);
        }
        else if (transform.position != _menuCameraPosition.position)
        {
            transform.position = _menuCameraPosition.position;
        }
    }

    private static void OnAttackedAnimation()
    {
        CameraShaker.Instance.ShakeOnce(2, 2, 0, 0.2f);
    }

//    private void OnGUI()
//    {
//        if (GUI.Button(new Rect(50, 50, 50, 50), "SHAKE"))
//        {
//            OnAttackedAnimation();
//        }
//    }
}