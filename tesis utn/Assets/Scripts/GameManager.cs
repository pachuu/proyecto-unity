﻿using System;
using System.Collections;
using System.IO;
using DataContainers;
using DefaultNamespace;
using Enums;
using Newtonsoft.Json;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    [Header("GameStatus")] public GameState GameMode = GameState.Menu;
    [Header("Player")] public PlayerSystem player;
    [SerializeField] private PlayerData _playerData;
    private PlayerData _playerSeasonData;
    private PlayerStatusController _playerStatus;
    [Space(10)] private string path;
    public bool OnPause;
    public bool TESTGAME = false;
    [SerializeField] private RockBase _rockBase;
    [Header("Game Events")] public Action OnInitGame;
    public Action OnInitMenu;
    public Action OnOldGameFound;
    public Action OnGameEnd;

    [SerializeField] private SpawnManager _spawnManager;

    [Space(20)] [Header("Menu")] [SerializeField]
    private GameObject _charMenuLight;

    private void Awake()
    {
        path = Path.Combine(Application.persistentDataPath, "data.json");
//        player = GameObject.FindWithTag("Player");
        _playerStatus = player.GetComponent<PlayerStatusController>();
        _playerStatus.OnDead += () => { LocalPersistence.SavePlayerData(SetNewPlayerStats(), path); };
        //Check if instance already exists
        if (Instance == null)

            //if not, set instance to this
            Instance = this;

        //If instance already exists and it's not this:
        else if (Instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one
            //instance of a GameManager.
            Destroy(gameObject);
    }

    private void Start()
    {
        if (TESTGAME)
        {
            InitGame();
            StartCoroutine(SearchAndResumeGame());
        }
        else
        {
            InitMenu();
        }
    }

    void SetupGame()
    {
    }


    public void InitMenu()
    {
        Time.timeScale = 1;
        GameMode = GameState.Menu;
        StartCoroutine(SearchAndResumeGame());
        OnInitMenu?.Invoke();
    }

    private void InitGame()
    {
        GameMode = GameState.Game;
        OnPause = false;
        OnInitGame?.Invoke();
    }

    public void SavePlayerData()
    {
        var playerData = _playerStatus._data;
        var gameStatus = new GameStatusData
        {
            baseHealth = _rockBase.Health,
            currentWave = _spawnManager._numWaves,
            player = playerData
        };
        LocalPersistence.SavePlayerData(gameStatus, path);
    }

    public void EndGame()
    {
        OnGameEnd?.Invoke();
        GameMode = GameState.Menu;
    }
    

    private IEnumerator SearchAndResumeGame(bool startGame = false)
    {
        var myWWW = new WWW(path);
        yield return myWWW;
        if (string.IsNullOrEmpty(myWWW.error))
        {
//            var jsonData = System.Text.Encoding.UTF8.GetString(myWWW.bytes, 3,
//                myWWW.bytes.Length - 3);
            var jsonData = myWWW.text;
            var persistedData = JsonConvert.DeserializeObject<GameStatusData>(jsonData);
            if (persistedData.player != null)
            {
                OnOldGameFound?.Invoke();
            }
            else
            {
                _playerData = SetNewPlayerStats();
                _rockBase.SetHealth(1000);
            }

            if (startGame)
            {
                _rockBase.SetHealth(persistedData.baseHealth);
                _spawnManager.SetWave(persistedData.currentWave);
                player.SetupPlayer(persistedData.player);
                InitGame();
            }

//            _playerStatus.InitStats(_playerData);
//            player.GetComponent<CombatController>().data = _playerData;
        }
    }

    private PlayerData SetNewPlayerStats()
    {
        return Instantiate(_playerData);
    }

    public void StartNewGame()
    {
        _playerData = SetNewPlayerStats();
        _rockBase.SetHealth(1000);
        _spawnManager.SetWave(0);
        player.SetupPlayer(_playerData);
        InitGame();
    }

    public void ContinueGame()
    {
        StartCoroutine(SearchAndResumeGame(true));
    }

    public void OnResumeButton()
    {
        Time.timeScale = 1;
        OnPause = false;
    }
}