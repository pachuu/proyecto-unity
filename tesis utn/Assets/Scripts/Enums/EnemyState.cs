﻿namespace Enums
{
    public enum EnemyState
    {
        None,
        Idle,
        Chase,
        Attacking
    }
}