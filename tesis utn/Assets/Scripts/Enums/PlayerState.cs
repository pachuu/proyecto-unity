﻿namespace Enums
{
    public enum PlayerState
    {
        Idle,
        Walking,
        Dead,
        Attacking,
        CheckingRange
        
    }
}