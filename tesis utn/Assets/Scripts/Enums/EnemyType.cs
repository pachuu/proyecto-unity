﻿namespace Enums
{
    public enum EnemyType
    {
        Melee = 1,
        Ranged = 2,
        Boss = 3
    }
}