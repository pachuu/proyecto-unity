﻿namespace Enums
{
    public enum PlayerType
    {
        Warrior = 1,
        Rogue = 2,
        Mage = 3
    }
}