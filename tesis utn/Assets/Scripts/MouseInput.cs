﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Characters.Scripts.Components;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInput : MonoBehaviour, IInputStream
{
    public Action<Vector3> OnNewPosition { get; set; }
    public Action<GameObject, Vector3> OnAttackButton { get; set; }
    public Action<GameObject, Vector3> OnInteractButton { get; set; }

    public Action<Vector3> OnPositionChanged { get; set; }
    private Camera mainCamera;

    private IDisposable leftClickStream;
    private IDisposable rightClickStream;
    private int fingerID = -1;

    private Vector3 ClickPosition { get; set; }

    private void Start()
    {
#if !UNITY_EDITOR
     fingerID = 0; 
 #endif
        //todo: comentar en producción. 
        //InitInputs();
    }

    public void InitInputs()
    {
        leftClickStream = Observable.EveryUpdate().Where(_ => Input.GetMouseButtonDown(0))
            .Subscribe(_ =>
            {
                if (EventSystem.current.IsPointerOverGameObject(fingerID)) return;
                if (GameManager.Instance.OnPause) return;
                // GUI Action

                var gObject = GetRaycastHit().transform.gameObject;
                if (gObject.layer == LayerMask.NameToLayer("Walkable"))
                    OnNewPosition?.Invoke(GetRaycastHit().point);
            });
        rightClickStream = Observable.EveryUpdate().Where(_ => Input.GetMouseButtonDown(1))
            .Subscribe(_ =>
            {
                if (EventSystem.current.IsPointerOverGameObject(fingerID)) return;
                if (GameManager.Instance.OnPause) return;
                // GUI Action
                
                var gObject = GetRaycastHit().transform.gameObject;
                if (gObject.layer == LayerMask.NameToLayer("Enemy"))
                    OnInteractButton?.Invoke(gObject, GetRaycastHit().point);
            });
    }

    public void StopInputs()
    {
        leftClickStream?.Dispose();
        rightClickStream?.Dispose();
    }


    //Cuando Hacemos click izquierdo (mover, mostrar datos, ect)
    private void SetClickPosition()
    {
        OnNewPosition(GetRaycastHit().point);
    }

    //Cuando hacemos click derecho (atacar, interactuar)
//    private void OnInteracted()
//    {
//        var obj = this.GetRaycastHit().transform.gameObject;
//        Debug.Log("obj clicked -> " + obj.tag);
//        OnInteractButton(obj);
//    }


    private Camera FindCamera()
    {
        return mainCamera ? mainCamera : Camera.main;
    }

    private RaycastHit GetRaycastHit()
    {
        RaycastHit hit;
        mainCamera = FindCamera();
        if (!Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out hit, 100)) return hit;
        if (!hit.transform) return hit;
        ClickPosition = hit.point;
        hit = hit;

        return hit;
    }
}