﻿using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace DefaultNamespace
{
    public static class LocalPersistence
    {
        public static T LoadData<T>(string path)
        {
            var data = JsonUtility.FromJson<T>(path);
            return data;
        }


        public static void SavePlayerData<T>(T data, string path)
        {
            var json = JsonConvert.SerializeObject(data);
            StreamWriter sw = File.CreateText(path);
            sw.Close();
            File.WriteAllText(path, json);
        }
    }
}