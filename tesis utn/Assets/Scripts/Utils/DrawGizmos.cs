﻿using DataContainers;
using UnityEngine;

namespace Utils
{
    public class DrawGizmos : MonoBehaviour
    {
        [SerializeField] private float _radius;

        [SerializeField] [ColorUsage(true, order = 0)]
        private Color _color;

        private PlayerStatusController _playerStatus;
        [SerializeField] private bool _drawSphere;
        [SerializeField] private bool _drawCircle;

        private void Awake()
        {
//            _playerStatus = GetComponent<PlayerStatusController>();
            _drawCircle = true;
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = this._color;
            if (_drawSphere)
                Gizmos.DrawWireSphere(transform.position, _radius);
            if (_drawCircle)
            {
                UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.up, _radius);
            }
        }
        #endif
    }
}