﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "SettingsData", menuName = "Game/Settings", order = 1)]
public class SettingsData : ScriptableObject
{
    [Header("Configuraciones generales del juego")]
    public bool music;
    public Device device;

}

public enum Device
{
    standalone = 1,
    mobile = 2
}
