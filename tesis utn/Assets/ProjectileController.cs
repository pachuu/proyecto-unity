﻿using System.Collections;
using System.Collections.Generic;
using Characters.Scripts.Components;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    [SerializeField] private RFX4_TransformMotion1 _collision;

    private CombatController _parent;

    // Use this for initialization
    void Start()
    {
        _collision.CollisionEnter += CollisionEnter;
    }

    public void SetController(CombatController c)
    {
        _parent = c;
    }

    private void CollisionEnter(object sender, RFX4_TransformMotion1.RFX4_CollisionInfo e)
    {
//        Debug.Log("colision on " + sender + " info-> " + e.Hit.collider.name);
        if (_parent != null)
        {
            var damageable = e.Hit.collider.gameObject.GetComponent<IReceiveDamage>();
            if (damageable != null && !damageable.dead)
                _parent.OnProjectileAttack(damageable);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}