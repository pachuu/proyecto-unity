﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Policy;
using UnityEngine;

public class ElectricHandSkill : MonoBehaviour, ISkill
{
    public PlayerStatusController _PlayerStatus { get; set; }
    public int amount { get; set; }

    private void OnEnable()
    {
        _PlayerStatus._data.Damage += amount;
    }

    private void OnDisable()
    {
        _PlayerStatus._data.Damage -= amount;
    }


    public void InitSkill(PlayerStatusController _player, int amount)
    {
        this._PlayerStatus = _player;
        this.amount = amount;
        gameObject.SetActive(true);
    }

    public void StopSkill()
    {
        gameObject.SetActive(false);
    }
}

public interface ISkill
{
    PlayerStatusController _PlayerStatus { get; set; }
    int amount { get; set; }
    void InitSkill(PlayerStatusController _player, int amount);
    void StopSkill();
}